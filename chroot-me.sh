#!/bin/sh

umount fake_root_directory/proc > /dev/null 2>&1

mount -o bind /proc fake_root_directory/proc && \
chroot --userspec=1000:1000 fake_root_directory /bin/sh


umount fake_root_directory/proc > /dev/null 2>&1
